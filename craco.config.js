const path = require('path');

module.exports = {
  webpack: {
    alias: {
      '@components': path.resolve(__dirname, 'src/common/components/'),
      '@styles': path.resolve(__dirname, 'src/common/styles/'),
      '@models': path.resolve(__dirname, 'src/common/models/'),
      '@pages': path.resolve(__dirname, 'src/pages/'),
      '@constants': path.resolve(__dirname, 'src/common/constants/'),
      '@store': path.resolve(__dirname, 'src/common/store/'),
      '@hooks': path.resolve(__dirname, 'src/common/hooks/'),
      '@hoc': path.resolve(__dirname, 'src/common/hoc/'),
    },
  },
};
