import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router-dom';
import axios from 'axios';

import { Car } from '@models';
import { act } from 'react-dom/test-utils';
import CarDetailsPage from './CarDetailsPage';

jest.mock('axios');

describe('<CarDetailsPage />', () => {
  let unmountElement: () => void;

  describe('no data loaded', () => {
    beforeEach(async () => {
      await act(async () => {
        const { unmount } = render(
          <MemoryRouter initialEntries={['/car/31131']}>
            <Route path="/car/31131">
              <CarDetailsPage />
            </Route>
          </MemoryRouter>,
        );
        unmountElement = unmount;
      });
    });

    afterEach(() => {
      unmountElement();
    });

    it('CarDetailsPage text rendered', () => {
      const title = screen.getByTestId('name-placeholder');
      expect(title).toBeInTheDocument();
    });

    it('CarDetailsPage avatar not loaded', () => {
      const title = screen.queryByTestId('car-avatar');
      expect(title).toBeNull();
    });
  });

  describe('with data loaded', () => {
    const mockedCar: Car = {
      stockNumber: 31131,
      manufacturerName: 'testM',
      modelName: 'testN',
      mileage: {
        number: 100,
        unit: 'km',
      },
      fuelType: 'Diesel',
      color: 'blue',
      pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    };

    beforeEach(async () => {
      (axios as any).get.mockImplementationOnce(
        () => Promise.resolve({ data: { car: mockedCar } }),
      );

      await act(async () => {
        const { unmount } = render(
          <MemoryRouter initialEntries={['/car/31131']}>
            <Route path="/car/31131">
              <CarDetailsPage />
            </Route>
          </MemoryRouter>,
        );

        unmountElement = unmount;
      });
    });

    afterEach(() => {
      unmountElement();
    });

    it('avatar rendered', async () => {
      const avatar = screen.getByTestId('car-avatar');
      expect(avatar).toBeInTheDocument();
    });

    it('header rendered manufacturer name', async () => {
      const header = screen.getByText(new RegExp(mockedCar.manufacturerName, 'i'));
      expect(header).toBeInTheDocument();
    });

    it('header rendered model name', async () => {
      const header = screen.getByText(new RegExp(mockedCar.modelName, 'i'));
      expect(header).toBeInTheDocument();
    });

    it('stock number rendered', async () => {
      const text = screen.getByText(new RegExp(mockedCar.stockNumber.toString(), 'i'));
      expect(text).toBeInTheDocument();
    });

    it('mileage number rendered', async () => {
      const text = screen.getByText(new RegExp(mockedCar.mileage.number.toString(), 'i'));
      expect(text).toBeInTheDocument();
    });

    it('mileage unit rendered', async () => {
      const text = screen.getByText(new RegExp(mockedCar.mileage.unit, 'i'));
      expect(text).toBeInTheDocument();
    });

    it('fuel type rendered', async () => {
      const text = screen.getByText(new RegExp(mockedCar.fuelType, 'i'));
      expect(text).toBeInTheDocument();
    });

    it('color rendered', async () => {
      const text = screen.getByText(new RegExp(mockedCar.color, 'i'));
      expect(text).toBeInTheDocument();
    });

    it('main text rendered', async () => {
      const text = screen.getByText('This car is currently available and can be delivered as soon as tomorrow morning. '
      + 'Please be aware that delivery times shown in this page are not definitive and may change due to bad weather conditions.');
      expect(text).toBeInTheDocument();
    });
  });
});
