import React, { FC, Fragment, useEffect } from 'react';
import { useStore } from 'effector-react';
import { useParams } from 'react-router-dom';

import {
  carDetailsStore, carRequestError, carRequestLoadingStore, getCarDetailsRequest,
} from '@store';
import { withErrorHandler } from '@hoc';
import { CarDetailsFavorite } from './Subs';

import './CarDetailsPage.scss';

const CarDetailsPage: FC = withErrorHandler(carRequestError, () => {
  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    getCarDetailsRequest(id);
  }, [id]);

  const {
    pictureUrl,
    manufacturerName,
    modelName,
    stockNumber,
    mileage,
    fuelType,
    color,
  } = useStore(carDetailsStore);

  const isCarDataLoading = useStore(carRequestLoadingStore);

  return (
    <section className="Details-page App__page">

      <div className="Details-page__avatar-wrapper">
        {pictureUrl && (
          <img className="Details-page__avatar" src={pictureUrl} alt={`${manufacturerName} ${modelName} avatar`} data-testid="car-avatar" />
        )}
      </div>

      <div className="Details-page__content-wrapper padding_l">
        <div className="Details-page__content">
          <div className="Details-page__details">
            {!isCarDataLoading ? (
              <Fragment>
                <h1 className="Details-page__name text text_l text_bold">
                  {manufacturerName}
                  {modelName}
                </h1>
                <h3 className="Details-page__details-info text text_m padding_l padding_bottom">
                  Stock #
                  {' '}
                  {stockNumber}
                  {' '}
                  -
                  {' '}
                  {mileage?.number}
                  {' '}
                  <span>{mileage?.unit}</span>
                  {' '}
                  -
                  {' '}
                  {fuelType}
                  {' '}
                  -
                  {' '}
                  {color}
                </h3>
                <p className="Details-page__text text text_s">
                  This car is currently available and can be delivered as soon as tomorrow morning.
                  Please be aware that delivery times shown in this page are not definitive and may
                  change due to bad weather conditions.
                </p>
              </Fragment>
            ) : (
              <Fragment>
                <div className="Details-page__placeholder-name" data-testid="name-placeholder" />
                <div className="Details-page__placeholder-details" />
                <div className="Details-page__placeholder-text" />
                <div className="Details-page__placeholder-text" />
                <div className="Details-page__placeholder-text" />
                <div className="Details-page__placeholder-text" />
              </Fragment>
            )}
          </div>

          <CarDetailsFavorite stockNumber={id} />
        </div>
      </div>
    </section>
  );
});

export default CarDetailsPage;
