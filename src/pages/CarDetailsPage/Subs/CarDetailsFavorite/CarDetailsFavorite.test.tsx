import React from 'react';
import { render, screen } from '@testing-library/react';

import CarDetailsFavorite from './CarDetailsFavorite';

describe('<CarDetailsFavorite />', () => {
  const props = {
    stockNumber: '101',
  };

  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(<CarDetailsFavorite {...props} />);
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  describe('isFavorite store false value', () => {
    it('isFavorite false text rendered', () => {
      const title = screen.getByText('If you like this car, click the button and save it in your collection of favorite items.');
      expect(title).toBeInTheDocument();
    });

    it('isFavorite false button rendered', () => {
      const title = screen.getByText('Save');
      expect(title).toBeInTheDocument();
    });
  });

  describe('favorite button toggle', () => {
    let favoriteBtnSave: HTMLElement;
    let favoriteBtnRemove: HTMLElement;

    beforeEach(() => {
      favoriteBtnSave = screen.getByText('Save');
      favoriteBtnSave.click();
      favoriteBtnRemove = screen.getByText('Remove');
    });

    afterEach(() => {
      favoriteBtnRemove.click();
    });

    it('favorite save button click adding to favorite', () => {
      expect(favoriteBtnRemove).toBeInTheDocument();
    });

    it('isFavorite true text rendered', () => {
      const title = screen.getByText('If you don\'t like this car anymore, click and remove it from your collection of favorite items.');
      expect(title).toBeInTheDocument();
    });

    it('favorite remove button click removes from favorite', () => {
      favoriteBtnRemove.click();
      expect(favoriteBtnSave).toBeInTheDocument();
    });
  });
});
