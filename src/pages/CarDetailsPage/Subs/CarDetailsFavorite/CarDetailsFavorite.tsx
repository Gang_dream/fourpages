import React, { FC, useCallback } from 'react';

import { useFavorites } from '@hooks';
import { addToFavorites, carRequestLoadingStore, removeFromFavorites } from '@store';
import { Button } from '@components';
import { withLoader } from '@hoc';

import './CarDetailsFavorite.scss';

interface Props {
  stockNumber: string;
}

const CarDetailsFavorite: FC<Props> = withLoader<Props>(
  carRequestLoadingStore, ({ stockNumber }) => {
    const { isFavorite } = useFavorites(stockNumber);

    const handleToggleToFavorites = useCallback((isFavoriteValue) => () => (isFavoriteValue
      ? removeFromFavorites(stockNumber.toString())
      : addToFavorites(stockNumber.toString())), [stockNumber]);

    return (
      <div className="Details-page-favorite padding_l border border_gray">
        <p className="Details-page__text text text_s padding_m padding_bottom">
          {isFavorite
            ? 'If you don\'t like this car anymore, click and remove it from your collection of favorite items.'
            : 'If you like this car, click the button and save it in your collection of favorite items.'}
        </p>

        <Button onClick={handleToggleToFavorites(isFavorite)}>{isFavorite ? 'Remove' : 'Save'}</Button>
      </div>

    );
  },
);

export default CarDetailsFavorite;
