import React, {
  FC, useCallback, useEffect, useMemo,
} from 'react';
import { useStore } from 'effector-react';

import { Button, Dropdown } from '@components';
import {
  applyFilters,
  carsRequestLoadingStore,
  colorsData,
  getColorsRequest,
  getManufacturersRequest,
  manufacturersData,
  setColorFilter,
  setManufacturerFilter,
} from '@store';
import { FilterDefaultValues } from '@constants';
import { withLoader } from '@hoc';

import './FiltersBlock.scss';

const FiltersBlock: FC = withLoader(carsRequestLoadingStore, () => {
  useEffect(() => {
    getColorsRequest();
  }, []);

  useEffect(() => {
    getManufacturersRequest();
  }, []);

  const colors = useStore(colorsData);
  const manufacturers = useStore(manufacturersData);

  const manufacturersNames = useMemo(() => manufacturers.map(({ name }) => name), [manufacturers]);

  const handleFilterClick = useCallback(() => applyFilters(), []);
  const handleColorChange = useCallback((color: string) => setColorFilter(color), []);
  const handleManufacturerChange = useCallback(
    (manufacturer: string) => setManufacturerFilter(manufacturer), [],
  );

  return (
    <aside className="Filters padding_l border border_gray">
      <Dropdown
        id="color-dropdown"
        title="Color"
        list={colors}
        onChange={handleColorChange}
        placeholder={FilterDefaultValues.COLOR}
      />
      <Dropdown
        id="manufacturer-dropdown"
        className="Filters__last-filter"
        title="Manufacturer"
        list={manufacturersNames}
        onChange={handleManufacturerChange}
        placeholder={FilterDefaultValues.MANUFACTURER}
      />
      <Button className="Filters__btn" onClick={handleFilterClick}>Filter</Button>
    </aside>
  );
});

export default FiltersBlock;
