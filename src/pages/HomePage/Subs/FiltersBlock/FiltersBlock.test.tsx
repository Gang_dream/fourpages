import React from 'react';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import axios from 'axios';

import { FilterDefaultValues } from '@constants';
import { getCarsRequest } from '@store';
import FiltersBlock from './FiltersBlock';

jest.mock('axios');

describe('<FiltersBlock />', () => {
  let unmountElement: () => void;

  describe('no data loaded', () => {
    beforeEach(() => {
      getCarsRequest();
      const { unmount } = render(<FiltersBlock />);
      unmountElement = unmount;
    });

    afterEach(() => {
      unmountElement();
    });

    it('color dropdown title rendered', () => {
      const title = screen.getByText('Color');
      expect(title).toBeInTheDocument();
    });

    it('color dropdown placeholder rendered', () => {
      const placeholder = screen.getAllByText(FilterDefaultValues.COLOR);
      expect(placeholder).toHaveLength(2);
    });

    it('manufacturer dropdown title rendered', () => {
      const title = screen.getByText('Manufacturer');
      expect(title).toBeInTheDocument();
    });

    it('manufacturer dropdown placeholder rendered', () => {
      const placeholder = screen.getAllByText(FilterDefaultValues.MANUFACTURER);
      expect(placeholder).toHaveLength(2);
    });

    it('filter button rendered', () => {
      const button = screen.getByText('Filter');
      expect(button).toBeInTheDocument();
    });

    it('loader rendered', () => {
      const loader = screen.getByTestId('loader');
      expect(loader).toBeInTheDocument();
    });
  });

  describe('with data loaded', () => {
    const mockedColors: string[] = ['blue'];

    beforeEach(async () => {
      (axios as any).get.mockImplementationOnce(() => Promise.resolve(
        {
          data: {
            colors: mockedColors,
          },
        },
      ));

      await act(async () => act(async () => {
        const { unmount } = render(<FiltersBlock />);
        unmountElement = unmount;
      }));
    });

    afterEach(() => {
      unmountElement();
    });

    it('mocked color rendered', () => {
      const color = screen.getByText(new RegExp(mockedColors[0], 'i'));
      expect(color).toBeInTheDocument();
    });

    it('no loader rendered', () => {
      const loader = screen.getByTestId('loader');
      expect(loader).toBeInTheDocument();
    });
  });
});
