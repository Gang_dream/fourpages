import React, { FC, useEffect, useMemo } from 'react';
import cn from 'classnames';
import { useStore } from 'effector-react';
import { v4 as uuid } from 'uuid';

import {
  carsStore,
  getCarsRequest,
  totalPageCountStore,
  totalCarsCountStore,
  currentPageStore,
  setNextPage,
  setFirstPage,
  setPreviousPage,
  setLastPage,
} from '@store';
import { Button, CarItem } from '@components';

import './CarsBlock.scss';

interface Props {
  className?: string;
}

const CarsBlock: FC<Props> = ({ className }) => {
  useEffect(() => {
    getCarsRequest();
  }, []);

  const cars = useStore(carsStore);
  const totalPageCount = useStore(totalPageCountStore);
  const totalCarsCount = useStore(totalCarsCountStore);
  const currentPage = useStore(currentPageStore);

  const classNames = useMemo(() => cn('CarsBlock', className), [className]);
  const carsCount = useMemo(() => cars.length, [cars]);
  const previousDisabled = useMemo(() => currentPage === 1, [currentPage]);
  const nextDisabled = useMemo(() => currentPage === totalPageCount, [totalPageCount, currentPage]);

  return (
    <section className={classNames}>
      <h2 className="text text_m text_bold">Available cars</h2>

      <p className="CarsBlock__current-count text text_m">
        Showing
        {' '}
        {carsCount}
        {' '}
        of
        {' '}
        {totalCarsCount}
        {' '}
        results (Page
        {' '}
        {currentPage}
        {' '}
        of
        {' '}
        {totalPageCount}
        )
      </p>

      <div className="CarsBlock__car-list">
        {cars.map((car) => <CarItem key={uuid()} car={car} className="CarsBlock__car-list-item" />)}
      </div>

      <div className="CarsBlock__navigation padding_l padding_top">
        <Button variant="link" onClick={setFirstPage} disabled={previousDisabled}>First</Button>
        <Button variant="link" onClick={setPreviousPage} disabled={previousDisabled}>Previous</Button>
        <p className="text text_s padding_m padding_horizontal">
          Page
          {' '}
          {currentPage}
          {' '}
          of
          {' '}
          {totalPageCount}
        </p>
        <Button variant="link" onClick={setNextPage} disabled={nextDisabled}>Next</Button>
        <Button variant="link" onClick={setLastPage} disabled={nextDisabled}>Last</Button>
      </div>
    </section>
  );
};

export default CarsBlock;
