import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router-dom';
import { act } from 'react-dom/test-utils';
import axios from 'axios';

import { Car } from '@models';
import CarsBlock from './CarsBlock';

jest.mock('axios');

describe('<CarsBlock />', () => {
  let unmountElement: () => void;

  describe('no data loaded', () => {
    beforeEach(async () => {
      await act(async () => {
        const { unmount } = render(
          <CarsBlock />,
        );
        unmountElement = unmount;
      });
    });

    afterEach(() => {
      unmountElement();
    });

    it('check header rendered', () => {
      const title = screen.getByText('Available cars');
      expect(title).toBeInTheDocument();
    });

    it('check default list loaded', () => {
      const title = screen.getByText('Showing 10 of 10 results (Page 1 of 1)');
      expect(title).toBeInTheDocument();
    });

    it('check pagination first rendered', () => {
      const title = screen.getByText('First');
      expect(title).toBeInTheDocument();
    });

    it('check pagination previous rendered', () => {
      const title = screen.getByText('Previous');
      expect(title).toBeInTheDocument();
    });

    it('check pagination next rendered', () => {
      const title = screen.getByText('Next');
      expect(title).toBeInTheDocument();
    });

    it('check pagination last rendered', () => {
      const title = screen.getByText('Last');
      expect(title).toBeInTheDocument();
    });

    it('check pagination default values', () => {
      const title = screen.getByText('Page 1 of 1');
      expect(title).toBeInTheDocument();
    });
  });

  describe('with data loaded', () => {
    const mockedCars: Car[] = [{
      stockNumber: 72373, manufacturerName: 'Mercedes-Benz', modelName: 'GT AMG', color: 'blue', mileage: { number: 100004, unit: 'km' }, fuelType: 'Diesel', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }, {
      stockNumber: 15629, manufacturerName: 'Fiat', modelName: 'Argenta', color: 'black', mileage: { number: 100100, unit: 'km' }, fuelType: 'Petrol', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }, {
      stockNumber: 26205, manufacturerName: 'Mercedes-Benz', modelName: 'GLA-Klasse', color: 'blue', mileage: { number: 100107, unit: 'km' }, fuelType: 'Diesel', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }, {
      stockNumber: 38793, manufacturerName: 'BMW', modelName: 'X4', color: 'white', mileage: { number: 100166, unit: 'km' }, fuelType: 'Diesel', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }, {
      stockNumber: 85892, manufacturerName: 'Porsche', modelName: 'Cayenne', color: 'black', mileage: { number: 100253, unit: 'km' }, fuelType: 'Diesel', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }, {
      stockNumber: 70343, manufacturerName: 'BMW', modelName: 'Z8', color: 'black', mileage: { number: 100287, unit: 'km' }, fuelType: 'Diesel', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }, {
      stockNumber: 87942, manufacturerName: 'Chrysler', modelName: 'Stratus', color: 'black', mileage: { number: 100441, unit: 'km' }, fuelType: 'Petrol', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }, {
      stockNumber: 83826, manufacturerName: 'Audi', modelName: 'A3', color: 'red', mileage: { number: 100516, unit: 'km' }, fuelType: 'Petrol', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }, {
      stockNumber: 45195, manufacturerName: 'Fiat', modelName: 'Talento', color: 'yellow', mileage: { number: 100621, unit: 'km' }, fuelType: 'Diesel', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }, {
      stockNumber: 92466, manufacturerName: 'Audi', modelName: 'S1', color: 'black', mileage: { number: 100622, unit: 'km' }, fuelType: 'Petrol', pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    }];
    const totalPageCount = 100;
    const totalCarsCount = 1000;

    beforeEach(async () => {
      (axios as any).get.mockImplementationOnce(
        () => Promise.resolve({ data: { cars: mockedCars, totalPageCount, totalCarsCount } }),
      );

      await act(async () => {
        const { unmount } = render(
          <MemoryRouter initialEntries={['/']}>
            <Route path="/">
              <CarsBlock />
            </Route>
          </MemoryRouter>,
        );
        unmountElement = unmount;
      });
    });

    afterEach(() => {
      unmountElement();
    });

    it('check total cars count and pages', () => {
      const title = screen.getByText(`Showing 10 of ${totalCarsCount} results (Page 1 of ${totalPageCount})`);
      expect(title).toBeInTheDocument();
    });

    it('check total page count', () => {
      const title = screen.getByText(`Page 1 of ${totalPageCount}`);
      expect(title).toBeInTheDocument();
    });
  });
});
