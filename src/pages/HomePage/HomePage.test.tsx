import React from 'react';
import { render, screen } from '@testing-library/react';

import HomePage from './HomePage';

describe('<HomePage />', () => {
  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(<HomePage />);
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('home page filter block rendered', () => {
    const filter = screen.getByText('Filter');
    expect(filter).toBeInTheDocument();
  });

  it('home page cars block rendered', () => {
    const filter = screen.getByText('Available cars');
    expect(filter).toBeInTheDocument();
  });
});
