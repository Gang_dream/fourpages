import React, { FC } from 'react';

import { CarsBlock, FiltersBlock } from './Subs';

import './HomePage.scss';

const HomePage: FC = () => (
  <main className="Home-page App__page padding_l">
    <FiltersBlock />
    <CarsBlock className="Home-page__cars-section" />
  </main>
);

export default HomePage;
