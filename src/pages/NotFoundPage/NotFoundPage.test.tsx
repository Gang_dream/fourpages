import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router-dom';

import NotFoundPage from './NotFoundPage';

describe('<NotFoundPage />', () => {
  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(
      <MemoryRouter initialEntries={['/404']}>
        <Route path="/404">
          <NotFoundPage />
        </Route>
      </MemoryRouter>,
    );
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('main logo rendered', () => {
    const logo = screen.getByAltText('Main logo');
    expect(logo).toBeInTheDocument();
  });

  it('notFoundPage main text rendered', () => {
    const title = screen.getByText('404 - Not Found');
    expect(title).toBeInTheDocument();
  });

  it('notFoundPage homepage link rendered', () => {
    const link = screen.getByText('homepage');
    expect(link).toBeInTheDocument();
  });

  it('notFoundPage homepage link url', () => {
    const link = screen.getByText('homepage');
    expect(link).toHaveAttribute('href', '/');
  });
});
