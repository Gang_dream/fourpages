import React, { FC } from 'react';

import { Link, MainLogo } from '@components';

import './NotFoundPage.scss';

const NotFoundPage: FC = () => (
  <section className="Not-found-page">
    <div className="Not-found-page__content">
      <MainLogo />
      <h1 className="text text_l text_bold">404 - Not Found</h1>
      <p className="text text_m padding_l padding_bottom">
        Sorry, the page you are looking for does not exist.
      </p>
      <p className="text text_m">
        You can always go back to the
        {' '}
        <Link url="/" className="text text_m">homepage</Link>
        {' '}
        .
      </p>
    </div>
  </section>
);

export default NotFoundPage;
