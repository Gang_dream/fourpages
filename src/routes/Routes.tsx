import React, { FC } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { RouterLinks } from '@constants';
import { CarDetailsPage, HomePage, NotFoundPage } from '@pages';

const Routes: FC = () => (
  <Switch>
    <Route
      path={RouterLinks.HOME}
      exact
      component={HomePage}
    />
    <Route
      path={RouterLinks.CAR_DETAILS}
      component={CarDetailsPage}
    />
    <Route
      path={RouterLinks.NOT_FOUND}
    >
      <NotFoundPage />
    </Route>
    <Route
      path="*"
    >
      <Redirect to={RouterLinks.NOT_FOUND} />
    </Route>
  </Switch>
);

export default Routes;
