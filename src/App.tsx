import React, { useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { Footer, Header, ErrorHint } from '@components';
import { closeAll, saveFavorites } from '@store';
import { Routes } from './routes';

import './App.scss';

const App = () => {
  useEffect(() => {
    window.onbeforeunload = () => saveFavorites();
    document.addEventListener('click', () => closeAll());
  }, []);

  return (
    <div className="App">
      <Router>
        <ErrorHint />
        <Header />
        <Routes />
        <Footer />
      </Router>
    </div>
  );
};

export default App;
