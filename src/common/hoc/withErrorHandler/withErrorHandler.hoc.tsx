import React, { FC, useEffect } from 'react';
import { Store } from 'effector';
import { useStore } from 'effector-react';
import { useHistory } from 'react-router-dom';

import { RequestErrorShape } from '@models';
import { setErrorVisibility } from '@store';

const withErrorHandler = <T, >(errorStore: Store<RequestErrorShape | null>, Component: FC<T>) => {
  const WrappedComponent = (props: T) => {
    const error = useStore(errorStore);
    const history = useHistory();

    useEffect(() => {
      if (error !== null) {
        if (error?.response?.data === 'Not Found' && error?.response?.status === 404) {
          history.push('/404');
        } else {
          setErrorVisibility(true);
        }
      }
    }, [error, history]);

    return (
      <Component {...props} />
    );
  };

  return WrappedComponent;
};

export default withErrorHandler;
