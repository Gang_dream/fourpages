import React, { FC } from 'react';
import { Store } from 'effector';
import { useStore } from 'effector-react';

import { LoadingWrapper } from '@components';

const withLoader = <T, >(loadingStore: Store<boolean>, Component: FC<T>) => {
  const WrappedComponent = (props: T) => {
    const isLoading = useStore(loadingStore);

    return (
      <LoadingWrapper isLoading={isLoading}>
        <Component {...props} />
      </LoadingWrapper>
    );
  };

  return WrappedComponent;
};

export default withLoader;
