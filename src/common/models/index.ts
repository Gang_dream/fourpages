export interface ManufacturerModel {
  name: string;
}

export interface Manufacturer {
  name: string;
  models: ManufacturerModel[];
}

export interface Car {
  stockNumber: number;
  manufacturerName: string;
  modelName: string;
  mileage: {
    number: number;
    unit: 'km' | 'mi'
  };
  fuelType: 'Diesel' | 'Petrol';
  color: string;
  pictureUrl: string;
}

export interface CarsResponseShape {
  cars: Car[];
  totalCarsCount: number;
  totalPageCount: number;
}

export interface ColorsResponseShape {
  colors: string[];
}

export interface ManufacturersResponseShape {
  manufacturers: Manufacturer[];
}

export interface CarsRequestShape {
  sort: 'asc' | 'des';
  page: number;
  color?: string;
  manufacturer?: string;
}

export interface DetailsResponseShape {
  car: Car;
}

export interface RequestErrorShape {
  response: {
    data: string;
    status: number;
    statusText: string;
  }
}

export interface DropdownStore {
  [key: string]: boolean
}
