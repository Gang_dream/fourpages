import { useStore } from 'effector-react';
import { favoriteStore } from '@store';

export const useFavorites = (stockNumber: string) => {
  const favorites = useStore(favoriteStore);

  const isFavorite = favorites.includes(stockNumber);

  return { isFavorite };
};
