import axios from 'axios';
import { createStore, createEffect, createEvent } from 'effector';

import {
  Car,
  DetailsResponseShape,
  CarsRequestShape,
  CarsResponseShape,
  ColorsResponseShape,
  Manufacturer,
  ManufacturersResponseShape,
  RequestErrorShape,
} from '@models';
import { Endpoint, FilterDefaultValues, SERVER_URL } from '@constants';

export const getColorsRequest = createEffect(async () => {
  const { data } = await axios.get<ColorsResponseShape>(`${SERVER_URL}${Endpoint.COLORS}`);

  return data;
});

export const getManufacturersRequest = createEffect(async () => {
  const { data } = await axios.get<ManufacturersResponseShape>(`${SERVER_URL}${Endpoint.MANUFACTURERS}`);

  return data;
});

export const getCarDetailsRequest = createEffect<string, DetailsResponseShape, RequestErrorShape>(
  async (stockNumber: string) => {
    const { data } = await axios.get<DetailsResponseShape>(`${SERVER_URL}${Endpoint.CARS}/${stockNumber}`);

    return data;
  },
);

export const setColorFilter = createEvent<string>('set color filter');
export const setManufacturerFilter = createEvent<string>('set manufacturer filter');
export const resetCars = createEvent();
export const setFirstPage = createEvent('set first page');
export const setPreviousPage = createEvent('set previous page');
export const setNextPage = createEvent('set next page');
export const setLastPage = createEvent('set last page');
export const setTotalPage = createEvent<number>('set total page');

export const applyFilters = createEvent('apply filters');

export const colorFilter = createStore<string>(FilterDefaultValues.COLOR)
  .on(setColorFilter, (_, newColor) => newColor);

export const manufactureFilter = createStore<string>(FilterDefaultValues.MANUFACTURER)
  .on(setManufacturerFilter, (_, newManufacturer) => newManufacturer);

export const totalPageCountStore = createStore<number>(1)
  .on(setTotalPage, (_, totalPageCount) => totalPageCount);

export const currentPageStore = createStore<number>(1)
  .on(setFirstPage, () => 1)
  .on(setPreviousPage, (currentPage) => (currentPage === 1 ? 1 : currentPage - 1))
  .on(setNextPage, (currentPage) => (
    totalPageCountStore.getState() === currentPage ? currentPage : currentPage + 1))
  .on(setLastPage, () => totalPageCountStore.getState());

export const getCarsRequest = createEffect(async () => {
  const stateColor = colorFilter.getState();
  const stateManufacturer = manufactureFilter.getState();

  const params: CarsRequestShape = {
    sort: 'asc',
    page: currentPageStore.getState(),
    color: stateColor === FilterDefaultValues.COLOR ? '' : stateColor,
    manufacturer: stateManufacturer === FilterDefaultValues.MANUFACTURER ? '' : stateManufacturer,
  };

  const { data } = await axios
    .get<CarsResponseShape>(`${SERVER_URL}${Endpoint.CARS}`, { params });

  return data;
});

getCarsRequest.done.watch(({ result: { totalPageCount } }) => setTotalPage(totalPageCount));

currentPageStore.watch(() => getCarsRequest());

applyFilters.watch(() => {
  if (currentPageStore.getState() === 1) {
    getCarsRequest();
  } else {
    setFirstPage();
  }
});

const emptyCarsList = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}];

export const carsStore = createStore<Car[]>(emptyCarsList as Car[])
  .on(getCarsRequest, () => emptyCarsList as Car[])
  .on(getCarsRequest.done, (_, { result: { cars } }) => cars);

export const totalCarsCountStore = createStore<number>(10)
  .on(getCarsRequest.done, (_, { result: { totalCarsCount } }) => totalCarsCount);

export const colorsData = createStore<string[]>([])
  .on(getColorsRequest.done, (_, { result: { colors } }) => colors);

export const manufacturersData = createStore<Manufacturer[]>([])
  .on(getManufacturersRequest.done, (_, { result: { manufacturers } }) => manufacturers);

export const carDetailsStore = createStore<Car>({} as Car)
  .on(getCarDetailsRequest.done, (_, { result: { car } }) => car);

export const carsRequestLoadingStore = createStore(false)
  .on(getCarsRequest, () => true)
  .on(getCarsRequest.done, () => false);

export const carRequestLoadingStore = createStore(false)
  .on(getCarDetailsRequest, () => true)
  .on(getCarDetailsRequest.done, () => false);

export const carRequestError = createStore<RequestErrorShape | null>(null)
  .on(getCarDetailsRequest.fail, (_, { error }) => error);
