import { createStore, createEvent } from 'effector';
import { without } from 'ramda';

export const addToFavorites = createEvent<string>('add car to favorite');
export const removeFromFavorites = createEvent<string>('remove car from favorite');
export const saveFavorites = createEvent('save favorites to localstorage');

const defaultFavorites = localStorage.getItem('favorites')?.split(',') || [];

export const favoriteStore = createStore<string[]>(defaultFavorites)
  .on(addToFavorites, (favorites, newItem) => [...favorites, newItem])
  .on(removeFromFavorites, (favorites, removedItem) => without([removedItem], favorites));

saveFavorites.watch(() => localStorage.setItem('favorites', favoriteStore.getState().join(',')));
