import { createEvent, createStore } from 'effector';

import { getCarsRequest, getColorsRequest, getManufacturersRequest } from '@store';

export const setErrorVisibility = createEvent<boolean>('set error visible');

export const isErrorVisibleStore = createStore(false)
  .on(getCarsRequest.fail, () => true)
  .on(getManufacturersRequest.fail, () => true)
  .on(getColorsRequest.fail, () => true)
  .on(setErrorVisibility, (_, value) => (value || true));
