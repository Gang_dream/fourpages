import { DropdownStore } from '@models';
import { createStore, createEvent } from 'effector';
import { assoc, dissoc, map } from 'ramda';

export const addOrSetDropdown = createEvent<{ key: string, value?: boolean }>('add dropdown');
export const removeDropdown = createEvent<string>('remove dropdown');
export const toggleDropdown = createEvent<string>('toggle dropdown');
export const closeAll = createEvent('close all dropdowns');

export const dropdownListStore = createStore<DropdownStore>({})
  .on(addOrSetDropdown, (state, { key, value = false }) => assoc(key, value, state))
  .on(removeDropdown, (state, key) => dissoc(key, state))
  .on(toggleDropdown, (state, key) => assoc(key, !state[key], state))
  .on(closeAll, (state) => map<DropdownStore, DropdownStore>(() => false, state));
