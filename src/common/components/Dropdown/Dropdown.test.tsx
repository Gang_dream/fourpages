import React from 'react';
import { render, screen } from '@testing-library/react';
import Dropdown from './Dropdown';

describe('<Dropdown />', () => {
  const dropdownItemTextPart = 'test';

  const props = {
    list: [`${dropdownItemTextPart}1`, `${dropdownItemTextPart}2`, `${dropdownItemTextPart}3`],
    placeholder: 'Not selected',
    title: 'test dropdown',
    onChange: jest.fn(),
  };

  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(<Dropdown {...props} />);
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('rendered proper count of dropdown items', () => {
    const dropdownItems = screen.getAllByText(new RegExp(`${dropdownItemTextPart}\\d{1}`, 'i'));
    expect(dropdownItems).toHaveLength(props.list.length);
  });

  it('rendered placeholder and default value in dropdown items list', () => {
    const placeholders = screen.getAllByText(new RegExp(props.placeholder, 'i'));
    expect(placeholders).toHaveLength(2);
  });

  it('rendered title', () => {
    const title = screen.getByText(new RegExp(props.title, 'i'));
    expect(title).toBeInTheDocument();
  });

  describe('click dropdown item', () => {
    let firstItem: HTMLElement;

    beforeEach(() => {
      firstItem = screen.getByText(new RegExp(`${dropdownItemTextPart}1`, 'i'));
      firstItem.click();
    });

    it('calls handler', () => {
      expect(props.onChange).toBeCalled();
    });

    it('removes placeholder', () => {
      const placeholder = screen.getAllByText(new RegExp(props.placeholder, 'i'));
      expect(placeholder).toHaveLength(1);
    });

    it('add item title to selected', () => {
      const placeholder = screen.getAllByText(new RegExp(`${dropdownItemTextPart}1`, 'i'));
      expect(placeholder).toHaveLength(2);
    });

    it('then clicking placeholder item gets back placeholder', () => {
      const placeholder = screen.getAllByText(new RegExp(props.placeholder, 'i'))[0];
      placeholder.click();
      const placeholders = screen.getAllByText(new RegExp(props.placeholder, 'i'));
      expect(placeholders).toHaveLength(2);
    });
  });
});
