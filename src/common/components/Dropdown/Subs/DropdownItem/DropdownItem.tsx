import React, { FC, useCallback, useMemo } from 'react';
import cn from 'classnames';

import { Button } from '@components';

import './DropdownItem.scss';

interface Props {
  value: string;
  current: string;
  onClick: () => void;
}

const DropdownItem: FC<Props> = ({ value, current, onClick }) => {
  const classNames = useMemo(() => cn(
    'Dropdown-item text text_s',
    { 'Dropdown-item_selected': value === current },
  ), [current, value]);

  const handleClick = useCallback(() => onClick(), [value, onClick]);

  return (
    <li className={classNames}>
      <Button className="Dropdown-item__btn padding_m" variant="no-back" onClick={handleClick}>
        {value}
      </Button>
    </li>
  );
};

export default DropdownItem;
