import React from 'react';
import { render, screen } from '@testing-library/react';
import DropdownItem from './DropdownItem';

describe('<DropdownItem />', () => {
  const props = {
    value: 'test value',
    current: 'not prop value',
    onClick: jest.fn(),
  };

  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(<DropdownItem {...props} />);
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('dropdown item renders with proper text', () => {
    const dropdownItem = screen.getByText(new RegExp(props.value, 'i'));
    expect(dropdownItem).toBeInTheDocument();
  });

  it('click handler call', () => {
    const dropdownItem = screen.getByText(new RegExp(props.value, 'i'));
    dropdownItem.click();
    expect(props.onClick).toBeCalled();
  });

  it('dropdown item not render current text unequals to value', () => {
    const dropdownItem = screen.queryByText(new RegExp(props.current, 'i'));
    expect(dropdownItem).toBeNull();
  });
});
