import React, {
  FC, useCallback, useMemo, useState, KeyboardEvent, useEffect, MouseEvent,
} from 'react';
import cn from 'classnames';
import { v4 as uuid } from 'uuid';
import { useStoreMap } from 'effector-react';

import { Button } from '@components';
import {
  addOrSetDropdown, dropdownListStore, removeDropdown, toggleDropdown,
} from '@store';
import { DropdownItem } from './Subs';

import './Dropdown.scss';

interface Props {
  id: string,
  className?: string;
  list: string[];
  placeholder?: string;
  title: string;
  onChange: (value: string) => void;
}

const Dropdown: FC<Props> = ({
  id,
  placeholder = 'None',
  list,
  title,
  className,
  onChange,
}) => {
  const [selection, setSelection] = useState(placeholder);

  useEffect(() => {
    addOrSetDropdown({ key: id });

    return () => { removeDropdown(id); };
  }, []);

  const isOpen = useStoreMap(
    {
      store: dropdownListStore,
      keys: [id],
      fn: (dropdownList, [key]) => dropdownList[key] || false,
    },
  );

  const selectClassNames = useMemo(
    () => cn('Dropdown', { Dropdown_open: isOpen }),
    [isOpen],
  );
  const wrapperClassNames = useMemo(() => cn('Dropdown__wrapper', className), [
    className,
  ]);
  const selectionValue = useMemo(() => selection || placeholder, [
    selection,
    placeholder,
  ]);
  const noneClassNames = useMemo(
    () => cn('Dropdown-item text text_s', {
      'Dropdown-item_selected': selection === placeholder,
    }),
    [placeholder, selection],
  );

  const handleDropdownClick = useCallback((e: MouseEvent) => {
    e.stopPropagation();
    toggleDropdown(id);
  }, [isOpen]);

  const handleKeyPress = useCallback(({ key }: KeyboardEvent) => {
    if ((key) === 'Enter') {
      toggleDropdown(id);
    }
  }, []);

  const handleSelection = useCallback(
    (currentSelection: string) => () => {
      setSelection(currentSelection);
      onChange(currentSelection);
    },
    [onChange],
  );

  return (
    <div
      className={wrapperClassNames}
      tabIndex={0}
      onClick={handleDropdownClick}
      onKeyDown={handleKeyPress}
      role="button"
    >
      <p className="Dropdown__header text text_s">{title}</p>
      <div className={selectClassNames}>
        <div className="Dropdown__trigger padding_m text text_s border border_gray">
          <span>{selectionValue}</span>
          <div className="Dropdown__arrow" />
        </div>
        <ul className="Dropdown__options border border_gray">
          <li className={noneClassNames}>
            <Button className="Dropdown-item__btn padding_m" variant="no-back" onClick={handleSelection(placeholder)}>
              {placeholder}
            </Button>
          </li>
          {list.map((value) => (
            <DropdownItem
              key={uuid()}
              value={value}
              current={selection}
              onClick={handleSelection(value)}
            />
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Dropdown;
