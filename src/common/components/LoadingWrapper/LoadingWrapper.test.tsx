import React from 'react';
import { render, screen } from '@testing-library/react';

import LoadingWrapper from './LoadingWrapper';

describe('<LoadingWrapper />', () => {
  const props = {
    isLoading: true,
  };

  const childText = 'Test child';

  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(
      <LoadingWrapper {...props}>
        <div>{childText}</div>
      </LoadingWrapper>,
    );
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('child rendered', () => {
    const child = screen.getByText(new RegExp(childText, 'i'));
    expect(child).toBeInTheDocument();
  });

  it('loader rendered', () => {
    const loader = screen.getByTestId('loader');
    expect(loader).toBeInTheDocument();
  });

  describe('renders with isLoading false value', () => {
    beforeEach(() => {
      props.isLoading = false;
    });

    afterEach(() => {
      props.isLoading = true;
    });

    it('child rendered with isLoading false value', () => {
      const child = screen.getByText(new RegExp(childText, 'i'));
      expect(child).toBeInTheDocument();
    });

    it('loader not rendered', () => {
      const loader = screen.getByTestId('loader');
      expect(loader).toBeInTheDocument();
    });
  });
});
