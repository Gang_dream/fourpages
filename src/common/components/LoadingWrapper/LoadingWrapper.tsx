import React, { FC, Fragment } from 'react';

import './LoadingWrapper.scss';

interface Props {
  isLoading: boolean;
}

const LoadingWrapper: FC<Props> = ({ children, isLoading = false }) => (
  <div className="Loading-wrapper">
    <Fragment>
      {children}
      {isLoading && (
        <div className="Loading-wrapper__veil" data-testid="loader">
          <div className="Loading-wrapper__loader">
            <div className="Loading-wrapper__loader-circle" />
            <div className="Loading-wrapper__loader-circle" />
          </div>
        </div>
      )}
    </Fragment>
  </div>
);

export default LoadingWrapper;
