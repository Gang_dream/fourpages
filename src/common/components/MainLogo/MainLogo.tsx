import React, { FC } from 'react';
import { Link } from '@components';

import './MainLogo.scss';

const MainLogo: FC = () => (
  <Link url="/">
    <img
      className="Main-logo"
      src="https://auto1-js-task-api--mufasa71.repl.co/images/logo.png"
      alt="Main logo"
    />
  </Link>
);

export default MainLogo;
