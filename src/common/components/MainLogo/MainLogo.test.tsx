import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router-dom';

import MainLogo from './MainLogo';

describe('<MainLogo />', () => {
  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(
      <MemoryRouter initialEntries={['/']}>
        <Route path="/">
          <MainLogo />
        </Route>
      </MemoryRouter>,
    );
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('MainLogo rendered', () => {
    const logo = screen.getByAltText('Main logo');
    expect(logo).toBeInTheDocument();
  });
});
