import React from 'react';
import { render, screen } from '@testing-library/react';

import Portal from './Portal';

describe('<Portal />', () => {
  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(
      <Portal>
        <div>Test child</div>
      </Portal>,
    );
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('portal child rendered', () => {
    const child = screen.getByText('Test child');
    expect(child).toBeInTheDocument();
  });
});
