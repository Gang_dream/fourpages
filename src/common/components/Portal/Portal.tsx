import { FC } from 'react';
import { createPortal } from 'react-dom';

const Portal: FC = ({ children }) => {
  const root = document.getElementById('root') || document.body;
  return (
    createPortal(
      children,
      root,
    )
  );
};

export default Portal;
