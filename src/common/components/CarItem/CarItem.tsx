import React, {
  FC, Fragment, useCallback, useMemo,
} from 'react';
import { isEmpty } from 'ramda';
import cn from 'classnames';

import { Car } from '@models';
import { Link, Button } from '@components';
import { useFavorites } from '@hooks';
import { addToFavorites, removeFromFavorites } from '@store';

import './CarItem.scss';

interface Props {
  car: Car;
  className?: string;
}

const CarItem: FC<Props> = ({ className, car }) => {
  const {
    manufacturerName, modelName, stockNumber, mileage, fuelType, color, pictureUrl,
  } = car;

  const { isFavorite } = useFavorites(stockNumber?.toString());

  const classNames = useMemo(() => cn('Car-item padding_m border border_gray', className), [className]);
  const favoriteIcon = useMemo(() => (isFavorite ? '♥' : '♡'), [isFavorite]);

  const handleFavoriteClick = useCallback(() => (isFavorite
    ? removeFromFavorites(stockNumber.toString())
    : addToFavorites(stockNumber.toString())),
  [isFavorite, stockNumber]);

  return (
    <div className={classNames}>
      <div className="Car-item__avatar-wrapper">
        {pictureUrl && (
          <img className="Car-item__avatar" src={pictureUrl} alt={`${manufacturerName} ${modelName}`} />
        )}
      </div>

      <div className="Car-item__content">
        {isEmpty(car) ? (
          <Fragment>
            <div className="Car-item__name-placeholder" />
            <div className="Car-item__name-placeholder" />
            <div className="Car-item__link-placeholder" />
          </Fragment>
        ) : (
          <Fragment>
            <div className="Car-item__name-container">
              <p className="Car-item__name text text_m text_bold">
                {manufacturerName}
                {' '}
                {modelName}
              </p>
              <Button variant="no-back" onClick={handleFavoriteClick}>{favoriteIcon}</Button>
            </div>
            <p className="Car-item__details text text_s">
              Stock #
              {' '}
              {stockNumber}
              {' '}
              -
              {' '}
              {mileage?.number}
              {' '}
              <span>{mileage?.unit}</span>
              {' '}
              -
              {' '}
              {fuelType}
              {' '}
              -
              {' '}
              {color}
            </p>
            <Link url={`/car/${stockNumber}`}>View details</Link>
          </Fragment>
        )}
      </div>
    </div>
  );
};

export default CarItem;
