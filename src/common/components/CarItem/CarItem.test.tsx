import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router-dom';

import { Car } from '@models';
import CarItem from './CarItem';

describe('<CarItem />', () => {
  const props: { car: Car } = {
    car: {
      manufacturerName: 'Mercedes-Benz',
      modelName: 'GL-Klasse',
      stockNumber: 38741,
      mileage: {
        number: 100254,
        unit: 'km',
      },
      fuelType: 'Petrol',
      color: 'green',
      pictureUrl: 'https://auto1-js-task-api--mufasa71.repl.co/images/car.svg',
    },
  };

  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(
      <MemoryRouter initialEntries={['/']}>
        <Route path="/">
          <CarItem {...props} />
        </Route>
      </MemoryRouter>,
    );
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('Render avatar', () => {
    const avatar = screen.getByAltText(`${props.car.manufacturerName} ${props.car.modelName}`);
    expect(avatar).toBeInTheDocument();
  });

  it('Render favorite button', () => {
    const button = screen.getByText('♡');
    expect(button).toBeInTheDocument();
  });

  it('Render car stock number', () => {
    const text = screen.getByText(new RegExp(props.car.stockNumber.toString(), 'i'));
    expect(text).toBeInTheDocument();
  });

  it('Render details link', () => {
    const link = screen.getByText('View details');
    expect(link).toBeInTheDocument();
  });

  it('Render petrol type', () => {
    const text = screen.getByText(new RegExp(props.car.fuelType, 'i'));
    expect(text).toBeInTheDocument();
  });

  it('Render manufacturer name', () => {
    const text = screen.getByText(new RegExp(props.car.fuelType, 'i'));
    expect(text).toBeInTheDocument();
  });

  it('Render model name', () => {
    const text = screen.getByText(new RegExp(props.car.fuelType, 'i'));
    expect(text).toBeInTheDocument();
  });

  it('Render milage', () => {
    const text = screen.getByText(new RegExp(props.car.mileage.number.toString(), 'i'));
    expect(text).toBeInTheDocument();
  });

  it('Render color', () => {
    const text = screen.getByText(new RegExp(props.car.color, 'i'));
    expect(text).toBeInTheDocument();
  });
});
