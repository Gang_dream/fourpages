import React from 'react';
import { render, screen } from '@testing-library/react';
import Button from './Button';

describe('<Button />', () => {
  const props = {
    onClick: jest.fn(),
  };

  describe('enabled button', () => {
    let button: HTMLElement;
    let unmountElement: () => void;

    beforeEach(() => {
      const { unmount } = render(<Button {...props}>Test</Button>);
      unmountElement = unmount;
      button = screen.getByText('Test');
    });

    afterEach(() => {
      unmountElement();
    });

    it('Renders button', () => {
      expect(button).toBeInTheDocument();
    });

    it('click handler call', () => {
      button.click();
      expect(props.onClick).toBeCalled();
    });
  });

  describe('disabled button', () => {
    let disabledButton: HTMLElement;
    let disabledUnmount: () => void;

    beforeEach(() => {
      const { unmount } = render(<Button {...props} disabled>Disabled</Button>);
      disabledUnmount = unmount;
      disabledButton = screen.getByText('Disabled');
    });

    afterEach(() => {
      disabledUnmount();
    });

    it('is disabled', () => {
      expect(disabledButton).toBeDisabled();
    });

    it('click handler not called', () => {
      disabledButton.click();
      expect(props.onClick).toBeCalledTimes(0);
    });
  });
});
