import React, { FC, useMemo } from 'react';
import cn from 'classnames';

import './Button.scss';

interface Props {
  className?: string;
  variant?: 'simple' | 'link' | 'no-back';
  onClick: () => void;
  disabled?: boolean;
}

const Button: FC<Props> = ({
  children, className, onClick, variant = 'simple', disabled = false,
}) => {
  const classNames = useMemo(() => cn(
    'Button',
    className,
    {
      'Button_link padding_m padding_horizontal': variant === 'link',
      Button_simple: variant === 'simple',
      'Button_no-back': variant === 'no-back',
    },
  ), [className, variant]);

  return (
    <button className={classNames} onClick={onClick} type="button" disabled={disabled}>{children}</button>
  );
};

export default Button;
