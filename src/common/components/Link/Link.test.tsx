import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router-dom';

import Link, { Props } from './Link';

describe('<Link />', () => {
  const props: Props = {
    url: 'test.com',
    variant: 'nav',
  };

  let unmountElement: () => void;

  describe('renders with nav variant', () => {
    beforeEach(() => {
      const { unmount } = render(<Link {...props}>Test link</Link>);
      unmountElement = unmount;
    });

    afterEach(() => {
      unmountElement();
    });

    it('Link rendered', () => {
      const link = screen.getByText('Test link');
      expect(link).toBeInTheDocument();
    });

    it('has props url', () => {
      const link = screen.getByText('Test link');
      expect(link).toHaveAttribute('href', props.url);
    });
  });

  describe('renders with routing variant', () => {
    beforeEach(() => {
      props.variant = 'routing';
      const { unmount } = render(
        <MemoryRouter initialEntries={['/']}>
          <Route path="/">
            <Link {...props}>Test link</Link>
          </Route>
        </MemoryRouter>,
      );
      unmountElement = unmount;
    });

    afterEach(() => {
      unmountElement();
    });

    it('Link rendered', () => {
      const link = screen.getByText('Test link');
      expect(link).toBeInTheDocument();
    });

    it('has props url', () => {
      const link = screen.getByText('Test link');
      expect(link).toHaveAttribute('href', `/${props.url}`);
    });
  });
});
