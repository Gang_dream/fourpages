import React, { FC } from 'react';
import cn from 'classnames';
import { Link as RouterLink } from 'react-router-dom';

import './Link.scss';

export interface Props {
  className?: string;
  url: string;
  variant?: 'nav' | 'routing';
}

const Link: FC<Props> = ({
  url, variant = 'routing', className, children,
}) => {
  const classNames = cn(
    'Link',
    variant === 'routing' ? 'Link_routing' : 'Link_nav',
    className,
  );

  return variant === 'nav' ? (
    <a className={classNames} href={url}>{children}</a>
  ) : (
    <RouterLink className={classNames} to={url}>{children}</RouterLink>
  );
};

export default Link;
