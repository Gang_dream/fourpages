import React, { FC } from 'react';

import { Link, MainLogo } from '@components';

import './Header.scss';

const Header: FC = () => (
  <header className="Header padding_l">
    <MainLogo />

    <nav className="Header__nav">
      <Link url="/" className="Header__link" variant="nav">Purchase</Link>
      <Link url="/" className="Header__link" variant="nav">My Orders</Link>
      <Link url="/" className="Header__link" variant="nav">Sell</Link>
    </nav>

    <div className="Header__burger"><span /></div>
  </header>
);

export default Header;
