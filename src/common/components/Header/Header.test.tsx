import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router-dom';

import Header from './Header';

describe('<Header />', () => {
  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(
      <MemoryRouter initialEntries={['/']}>
        <Route path="/">
          <Header />
        </Route>
      </MemoryRouter>,
    );
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('main logo rendered', () => {
    const logo = screen.getByAltText('Main logo');
    expect(logo).toBeInTheDocument();
  });

  it('purchase link rendered', () => {
    const link = screen.getByText('Purchase');
    expect(link).toBeInTheDocument();
  });

  it('my Orders link rendered', () => {
    const link = screen.getByText('My Orders');
    expect(link).toBeInTheDocument();
  });

  it('sell link rendered', () => {
    const link = screen.getByText('Sell');
    expect(link).toBeInTheDocument();
  });
});
