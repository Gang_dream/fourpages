import React, { FC } from 'react';

import './Footer.scss';

const Footer: FC = () => (
  <footer className="Footer">
    <p className="Footer__text text text_m">© AUTO1 Group 2021</p>
  </footer>
);

export default Footer;
