import React from 'react';
import { render, screen } from '@testing-library/react';

import Footer from './Footer';

describe('<Footer />', () => {
  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(<Footer />);
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('footer text rendered', () => {
    const title = screen.getByText('© AUTO1 Group 2021');
    expect(title).toBeInTheDocument();
  });
});
