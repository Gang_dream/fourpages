import React from 'react';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import { setErrorVisibility } from '@store';
import ErrorHint from './ErrorHint';

describe('<ErrorHint />', () => {
  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(<ErrorHint />);
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });

  it('no title rendered', () => {
    const title = screen.queryByText('We are having some troubles with our server, please try again later');
    expect(title).toBeNull();
  });

  it('no reload button rendered', () => {
    const btn = screen.queryByText('Reload');
    expect(btn).toBeNull();
  });

  describe('renders with true isErrorVisibleStore value', () => {
    beforeEach(() => {
      act(() => {
        setErrorVisibility(true);
      });
    });

    afterEach(() => {
      act(() => {
        setErrorVisibility(false);
      });
    });

    it('rendered title', () => {
      const title = screen.getByText('We are having some troubles with our server, please try again later');
      expect(title).toBeInTheDocument();
    });

    it('rendered reload button', () => {
      const btn = screen.getByText('Reload');
      expect(btn).toBeInTheDocument();
    });
  });
});
