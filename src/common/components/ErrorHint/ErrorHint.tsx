import React, { FC, useCallback } from 'react';
import { useStore } from 'effector-react';

import { Portal, Button } from '@components';
import { isErrorVisibleStore } from '@store';

import './ErrorHint.scss';

const ErrorHint: FC = () => {
  const isErrorVisible = useStore(isErrorVisibleStore);

  const handleReload = useCallback(() => window.location.reload(), []);

  return (
    <Portal>
      {isErrorVisible && (
        <div className="Error-hint__wrapper">
          <div className="Error-hint__message padding_s">
            <p className="Error-hint__message-text text text_s">
              We are having some troubles with our server, please try again later
            </p>
          </div>

          <Button className="Error-hint__reload-btn" onClick={handleReload}>Reload</Button>
        </div>
      )}
    </Portal>
  );
};

export default ErrorHint;
