export enum RouterLinks {
  HOME = '/',
  CAR_DETAILS = '/car/:id',
  NOT_FOUND = '/404'
}

export const SERVER_URL = 'https://auto1-mock-server.herokuapp.com/api/';

export enum Endpoint {
  COLORS = 'colors',
  MANUFACTURERS = 'manufacturers',
  CARS = 'cars'
}

export enum FilterDefaultValues {
  COLOR = 'All car colors',
  MANUFACTURER = 'All manufacturers'
}
