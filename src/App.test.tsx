import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

describe('<App />', () => {

  let unmountElement: () => void;

  beforeEach(() => {
    const { unmount } = render(<App />);
    unmountElement = unmount;
  });

  afterEach(() => {
    unmountElement();
  });
  it('renders heading', () => {
    const headingNav = screen.getByAltText('Main logo');
    expect(headingNav).toBeInTheDocument();
  });

  it('renders footer list', () => {
    const carsList = screen.getByText('Available cars');
    expect(carsList).toBeInTheDocument();
  });

});
